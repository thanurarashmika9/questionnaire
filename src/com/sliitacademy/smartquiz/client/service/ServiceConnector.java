package com.sliitacademy.smartquiz.client.service;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.ServiceFactory.ServiceTypes;
import com.sliitacademy.smartquiz.common.service.SuperService;
import com.sliitacademy.smartquiz.common.service.custom.AdminService;
import com.sliitacademy.smartquiz.common.service.custom.CustomerService;
import com.sliitacademy.smartquiz.common.service.custom.QuestionService;
import com.sliitacademy.smartquiz.common.service.custom.SubmissionService;

/**Service Connector Creates an Instance of Service Classes With Singleton Design Pattern*/
public class ServiceConnector implements ServiceFactory {
	
	/**
	 * ServiceConnector instance created as a Static to be accessed from the client
	 * to access Services
	 */
	private static ServiceConnector connector;
	
	/** Service instance for Customer process handling */ 
	private CustomerService customerService;
	/** Service instance for Administrator process handling */
	private AdminService adminService;
	/** Service instance for Questionnaire related process handling */
	private QuestionService questionService;
	/** Service instance for Answer Submission related process handling */
	private SubmissionService submissionService;
	
	/**
	 * Here the Connection is created between Client and Server through RMI
	 * Service instances are initialized inside the constructor
	 * */
	public ServiceConnector() {
		try {
			ServiceFactory factory = (ServiceFactory)Naming.lookup("rmi://localhost:8989/SMART_QUIZ");
			customerService = factory.getService(ServiceTypes.CUSTOMER);
			adminService = factory.getService(ServiceTypes.ADMIN);
			questionService = factory.getService(ServiceTypes.QUESTION);
			submissionService = factory.getService(ServiceTypes.SUBMISSION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** Instance of ServiceConnector class can be created though this method
	 * Singleton Design Pattern is used here
	 * @return ServiceConnector Instance
	 * */
	public static ServiceConnector getInstance() {
		if (null == connector) {
			connector = new ServiceConnector();
		}
		return connector;
	}

	/** Object of the Required Service can be accessed with this method 
	 * @param serviceTypes required Service type
	 * @return Initialized object of the Required Service type<br>
	 * <b>Customer Service</b><br>
	 * <b>Admin Service</b><br>
	 * <b>Question Service</b><br>
	 * <b>Submission Service</b>
	 * */
	@Override
	public <T extends SuperService> T getService(ServiceTypes serviceTypes) throws Exception {
		switch (serviceTypes) {
		case CUSTOMER:
			return (T) customerService;
		case ADMIN:
			return (T) adminService;
		case QUESTION:
			return (T) questionService;
		case SUBMISSION:
			return (T) submissionService;
		default:
			return null;
		}
	}
}
