package com.sliitacademy.smartquiz.client.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * <h2>Final Page Functions are implemented here</h2>
 */
public class FinalPageController implements Initializable {

	/** Label for User name */
    @FXML
    private Label txtName;

	/** Sign off Button */
    @FXML
    private JFXButton btnSignOff;
    
	/** Variable to store Username */
    static String userName;
    
	/** Default Constructor */
    public FinalPageController() {
		
	}

	/** Action for Sign off Button Click Event<br>
	 * Signoff function is Implemented here<br>
	 * User signs off and Redirected to the main page*/
    @FXML
    public void signOff() {
		try {
    		Stage primaryStage = (Stage)btnSignOff.getScene().getWindow();
			primaryStage.close();
			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/mainView.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }

    /** Initialize Method 
	 * this will initialize the Final page
	 * User name is loaded to the view on Initializing
	 * @param location URL for the Location
	 * @param resources Resource Bundle
	 * */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtName.setText("Dear " + userName);
	}
}
