package com.sliitacademy.smartquiz.client.controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.custom.QuestionService;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * <h2>New Question Page Functions are implemented here</h2>
 */
public class NewQuestionController implements Initializable {
	
	/** Question Dto Stores Question Details */
	private static QuestionDto questionDto;
	
	/** Question Description Text Area */
	@FXML
    private JFXTextArea txtQuestionDesc;

	/** Text field for Answer 1 */
    @FXML
    private JFXTextField txtAnswer1;

    /** Text field for Answer 2 */
    @FXML
    private JFXTextField txtAnswer2;

    /** Text field for Answer 3 */
    @FXML
    private JFXTextField txtAnswer3;

    /** Text field for Answer 4 */
    @FXML
    private JFXTextField txtAnswer4;

    /** Text field for Answer 5 */
    @FXML
    private JFXTextField txtAnswer5;

	/** Cancel Button */
    @FXML
    private JFXButton btnCancel;

	/** Save Button */
    @FXML
    private JFXButton btnSave;

	/** Question Service Instance */
    private QuestionService questionService;
    
	/** Action for Cancel Button Click Event<br>
	 * This Cancels the New Question Saving Process
	 * @param event Action Event of Cancel Button */
    @FXML
    public void cancel(ActionEvent event) {
    	try {
    		questionDto = null;
    		Stage primaryStage = (Stage)btnCancel.getScene().getWindow();
    		primaryStage.close();
    		Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/questionSummary.fxml"));
    		Stage stage = new Stage();
    		Scene scene = new Scene(root);
    		stage.setScene(scene);
    		stage.show();
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/** Action Event for Save Button Click Event<br>
	 * This Saves a New Question or Updates an Existing Question
	 * @param event Action event of Save Button
	 *  */
    @FXML
    public void saveQuestion(ActionEvent event) {
    	try {
    		boolean isSave = null == questionDto;
    		Integer qid = 0;
    		if (!isSave) {
    			qid = questionDto.getId();
    		}
    		
    		QuestionDto questionDto = new QuestionDto();
    		questionDto.setId(qid);
    		questionDto.setQuestion(txtQuestionDesc.getText());
    		questionDto.setAnswer1(txtAnswer1.getText());
    		questionDto.setAnswer2(txtAnswer2.getText());
    		questionDto.setAnswer3(txtAnswer3.getText());
    		questionDto.setAnswer4(txtAnswer4.getText());
    		questionDto.setAnswer5(txtAnswer5.getText());

    		questionService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.QUESTION);
    		boolean isSaved = isSave  ? questionService.saveQuestion(questionDto) : questionService.updateQuestion(questionDto);
    		Alert alert = new Alert(isSaved ? AlertType.INFORMATION : AlertType.ERROR);
    		alert.setContentText(isSaved ? (isSave ? "Question Saved Successfully" : "Question Updated Successfully") : "Failed to Save Question");
    		txtQuestionDesc.setText("");
    		txtAnswer1.setText("");
    		txtAnswer2.setText("");
    		txtAnswer3.setText("");
    		txtAnswer4.setText("");
    		txtAnswer5.setText("");
    		Optional<javafx.scene.control.ButtonType> result = alert.showAndWait();
    		if (result.get() == javafx.scene.control.ButtonType.OK){
    			Stage primaryStage = (Stage) btnCancel.getScene().getWindow();
        		primaryStage.close();
    			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/questionSummary.fxml"));
    			Stage stage = new Stage();
    			Scene scene = new Scene(root);
    			stage.setScene(scene);
    			stage.show();    			
    		}
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
	/** This method initilalizes questionDto with the Question Details
	 * Entered by the User During Save or Update Process
	 * @param dto Question Details
	 *  */
    public static void setData(QuestionDto dto) {
    	questionDto = dto;
    }
    
	/** Default Constructor */
    public NewQuestionController() {
    	
    }


    /** Initialize Method <br>
	 * this will initialize the Question Saving page<br>
	 * Question Form will be loaded with the values During Update process and Remains Blank for a New Question
	 * @param location URL for the Location
	 * @param resources Resource Bundle
	 * */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		if (null != questionDto) {
			txtQuestionDesc.setText(questionDto.getQuestion());
			txtAnswer1.setText(questionDto.getAnswer1());
			txtAnswer2.setText(questionDto.getAnswer2());
			txtAnswer3.setText(questionDto.getAnswer3());
			txtAnswer4.setText(questionDto.getAnswer4());
			txtAnswer5.setText(questionDto.getAnswer5());
		}
	}
}
