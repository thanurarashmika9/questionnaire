package com.sliitacademy.smartquiz.client.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXButton.ButtonType;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.custom.QuestionService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * <h2>Question Summary Page Functions are implemented here</h2>
 */
public class QuestionSummaryController implements Initializable {
	
	/** List which Stores Question Details */
	private List<QuestionDto> questionsList = null;
	
	/** Question Details for the Table View are Assigned to this Observable List */
	private ObservableList<QuestionDto> tvObservableList = null;
	
	/** Table View for Question View */
	@FXML
    private TableView<QuestionDto> tblQuestionView;
	
	/** Add New Button */
	@FXML
    private JFXButton btnAddNew;

	/** Instance of Question Service */
	private QuestionService questionService;
	
	/** Action Event for Add new Button Click Event
	 * @param event Action Event for the Add New Button
	 *  */
    @FXML
    public void addNew(ActionEvent event) {
    	try {
    		Stage primaryStage = (Stage) btnAddNew.getScene().getWindow();
    		primaryStage.close();
    		NewQuestionController.setData(null);
			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/newQuestion.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }

	/** Default Constructor */
	public QuestionSummaryController() {
		
	}

    /** Initialize Method <br>
	 * this will initialize the Question Summary page<br>
	 * This is the Final Page of the Questionnaire
	 * @param location URL for the Location
	 * @param resources Resource Bundle
	 * */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
        loadView();
	}
	
	/** Details for the Question Table View is loaded here */
	private void loadView() {
		getAllQuestions();
        tblQuestionView.setItems(tvObservableList);

        tblQuestionView.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        tblQuestionView.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("question"));

        addButtonToTable();
	}
	
	/** All Questions are loaded from the server through this method<br>
	 * Then they are arranged in the Observable list so that it can be loaded to the Table view<br>
	 **/
    private void getAllQuestions() {
    	try {
    		questionService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.QUESTION);
    		questionsList = questionService.getAllQuestions();
    		tvObservableList = FXCollections.observableArrayList(questionsList);
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/** Here the Data is Managed and created rows of the Table>br>
	 * Delete, Update buttons are also added to Each and every Table row for the Operations  */
    private void addButtonToTable() {
        TableColumn<QuestionDto, Void> colBtn = new TableColumn("Action");
        TableColumn<QuestionDto, Void> colBtn2 = new TableColumn("Action");

        Callback<TableColumn<QuestionDto, Void>, TableCell<QuestionDto, Void>> cellFactory = new Callback<TableColumn<QuestionDto, Void>, TableCell<QuestionDto, Void>>() {
            @Override
            public TableCell<QuestionDto, Void> call(final TableColumn<QuestionDto, Void> param) {
                final TableCell<QuestionDto, Void> cell = new TableCell<QuestionDto, Void>() {

                    private final Button btn = new Button("View /Update");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                        	try {
                    			QuestionDto data = getTableView().getItems().get(getIndex());
                    			NewQuestionController.setData(data);
                    			Stage primaryStage = (Stage) btnAddNew.getScene().getWindow();
                        		primaryStage.close();
                    			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/newQuestion.fxml"));
                    			Stage stage = new Stage();
                    			Scene scene = new Scene(root);
                    			stage.setScene(scene);
                    			stage.show();
                        	} catch (IOException e) {
                    			e.printStackTrace();
                    		}
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        Callback<TableColumn<QuestionDto, Void>, TableCell<QuestionDto, Void>> cellFactory2 = new Callback<TableColumn<QuestionDto, Void>, TableCell<QuestionDto, Void>>() {
            @Override
            public TableCell<QuestionDto, Void> call(final TableColumn<QuestionDto, Void> param) {
                final TableCell<QuestionDto, Void> cell = new TableCell<QuestionDto, Void>() {

                    private final Button btn = new Button("Delete");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                        	try {
                        		Alert alert1 = new Alert(AlertType.CONFIRMATION);
                        		alert1.setContentText("Do you want to Delete this Question?");
                        		Optional<javafx.scene.control.ButtonType> result = alert1.showAndWait();
                        		if (result.get() == javafx.scene.control.ButtonType.OK){
                        			QuestionDto data = getTableView().getItems().get(getIndex());
                            		boolean success = questionService.deleteQuestion(data.getId());
                            		Alert alert2 = new Alert(success ? AlertType.INFORMATION : AlertType.ERROR);
                            		alert2.setContentText(success ? "Question Deleted Successfully" : "Failed to Delete Question");
                            		Optional<javafx.scene.control.ButtonType> result2 = alert2.showAndWait();
                            		if (result2.get() == javafx.scene.control.ButtonType.OK) {
                            			Stage primaryStage = (Stage) btnAddNew.getScene().getWindow();
                            			primaryStage.close();
                            			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/questionSummary.fxml"));
                            			Stage stage = new Stage();
                            			Scene scene = new Scene(root);
                            			stage.setScene(scene);
                            			stage.show();
                            		}
                        		}
                        	} catch (Exception e) {
                        		e.printStackTrace();
                        	}
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        
        colBtn.setCellFactory(cellFactory);
        colBtn2.setCellFactory(cellFactory2);

        tblQuestionView.getColumns().add(colBtn);
        tblQuestionView.getColumns().add(colBtn2);

    }	
}
