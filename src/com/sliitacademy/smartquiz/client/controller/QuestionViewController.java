package com.sliitacademy.smartquiz.client.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import com.sliitacademy.smartquiz.client.dto.AnswerDto;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.client.dto.ResultDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.custom.AdminService;
import com.sliitacademy.smartquiz.common.service.custom.QuestionService;
import com.sliitacademy.smartquiz.common.service.custom.SubmissionService;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * <h2>User's/ Customer's Question View Page Functions are implemented here</h2>
 */
public class QuestionViewController implements Initializable {
	/** Question Details are Stored here */
	private List<QuestionDto> questionsList = null;
	
	/** This is to maintain the current Question index */
	private Integer count = 0;
	
	/** Previous Question Navigation Button */
	@FXML
    private JFXButton btnPrevious;

	/** Next Question Navigation Button */
    @FXML
    private JFXButton btnNext;

	/** Textarea for Question Description */
    @FXML
    private JFXTextArea txtQuestion;

	/** Checkbox for Answer 1 */
    @FXML
    private JFXCheckBox chkAnswer1;

    /** Checkbox for Answer 2 */
    @FXML
    private JFXCheckBox chkAnswer2;

    /** Checkbox for Answer 3 */
    @FXML
    private JFXCheckBox chkAnswer3;

    /** Checkbox for Answer 4 */
    @FXML
    private JFXCheckBox chkAnswer4;

    /** Checkbox for Answer 5 */
    @FXML
    private JFXCheckBox chkAnswer5;
    
	/** Current View Heading is mentioned here */
    @FXML
    private Label lblLabel;
    
	/** This pane is used to load all the questions Dynamically */
    @FXML
    private AnchorPane innerPane;
    
	/** Current Question is Stored Here */
    private QuestionDto question;
    
	/** Answers of the User is stored in this list */
    private List<AnswerDto> answers = new ArrayList<>();
    
	/** Instance of Question Service */
    private QuestionService questionService;
    
	/** Instance of Submission Service */
    private SubmissionService submissionService;
    
	/** Instance of Admin Service */
    private AdminService adminService;
    
	/** Id of the Question */
    private int questionId;
    
	/** Username is Stored in this variable */
    static String userName;
    
	/** User Id is Stored in this variable */
    static int userId;

	/** Action Event for the Next Question Button click event<br>
	 * This navigates to the Next Question */
    @FXML
    public void nextQuestion() {
    	try {
    		if (adminService.isSessionExpired(String.valueOf(userId))) {
    			Alert alert1 = new Alert(AlertType.WARNING);
        		alert1.setContentText("Your Session has been Expired");
        		Optional<javafx.scene.control.ButtonType> result = alert1.showAndWait();
        		if (result.get() == javafx.scene.control.ButtonType.OK){
        			FinalPageController.userName = userName;
    				lblLabel.setText("Thank You");
        			AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/finalPage.fxml"));
    				btnNext.setVisible(false);
    				btnPrevious.setVisible(false);
    				if (!innerPane.getChildren().isEmpty()) {
    					innerPane.getChildren().clear();
    					innerPane.getChildren().add(pane);
    				}
        		}
    		} else {
	    		if (answers.size() == count) {
	    			Alert alert = new Alert(AlertType.WARNING);
	    			alert.setContentText("Please Provide an Answer");
	    			alert.show();
	    		} else {
		    		count++;
		        	setQuestion(true);
		        	btnPrevious.setText("Previous");
	    		}
    		}
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/** Action Event for the Previous Question Button click event<br> 
	 * This navigates to the Previous Question
	 * */
    @FXML
    void previousQuestion() {
    	try {
    		System.out.println(count);
    		if (count == 0) {
    			Stage primaryStage = (Stage)btnPrevious.getScene().getWindow();
    			primaryStage.close();
    			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/singupForm.fxml"));
    			Stage stage = new Stage();
    			Scene scene = new Scene(root);
    			stage.setScene(scene);
    			stage.show();
    		} else if (count == 1) {
    			btnPrevious.setText("Back");
    			count--;
    			setQuestion(false);
    		} else {
    			count--;
    			setQuestion(false);
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

	/** Checks the Answer 1 Checkbox and Unchecks the Others */
    @FXML
    void validateAnswer1() {
    	if (chkAnswer1.isSelected()) {
        	addAnswer(1);
    		chkAnswer2.setSelected(false);
    		chkAnswer3.setSelected(false);
    		chkAnswer4.setSelected(false);
    		chkAnswer5.setSelected(false);
    	} else {
        	addAnswer(0);
    	}
    }
    
    /** Checks the Answer 2 Checkbox and Unchecks the Others */
    @FXML
    void validateAnswer2() {
    	if (chkAnswer2.isSelected()) {
        	addAnswer(2);
    		chkAnswer1.setSelected(false);
    		chkAnswer3.setSelected(false);
    		chkAnswer4.setSelected(false);
    		chkAnswer5.setSelected(false);
    	} else {
        	addAnswer(0);
    	}
    }
    
    /** Checks the Answer 3 Checkbox and Unchecks the Others */
    @FXML
    void validateAnswer3() {
    	if (chkAnswer3.isSelected()) {
    		addAnswer(3);
    		chkAnswer2.setSelected(false);
    		chkAnswer1.setSelected(false);
    		chkAnswer4.setSelected(false);
    		chkAnswer5.setSelected(false);
    	} else {
    		addAnswer(0);
    	}
    }
    
    /** Checks the Answer 4 Checkbox and Unchecks the Others */
    @FXML
    void validateAnswer4() {
    	if (chkAnswer4.isSelected()) {
    		addAnswer(4);
    		chkAnswer2.setSelected(false);
    		chkAnswer3.setSelected(false);
    		chkAnswer1.setSelected(false);
    		chkAnswer5.setSelected(false);
    	} else {
    		addAnswer(0);
    	}
    }
    
    /** Checks the Answer 5 Checkbox and Unchecks the Others */
    @FXML
    void validateAnswer5() {
    	if (chkAnswer5.isSelected()) {
    		addAnswer(5);
    		chkAnswer2.setSelected(false);
    		chkAnswer3.setSelected(false);
    		chkAnswer4.setSelected(false);
    		chkAnswer1.setSelected(false);
    	} else {
    		addAnswer(0);
    	}
    }
    
	/** Answers are added to the answers list with this method
	 * @param answerId No of the Answer */
    private void addAnswer(int answerId) {
    	AnswerDto answer = new AnswerDto();
    	answer.setQuestionId(questionId);
    	answer.setAnswer(answerId);
    	if (answers.isEmpty()) {
    		answers.add(answer);
    	} else {
    		answers.remove(count);
    		answers.add(count, answer);
    	}
    }
    
	/** Defalut Constructor<br>
	 * Initialized the service instances
	 *  */
    public QuestionViewController() {
    	try {
    		questionService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.QUESTION);
			questionsList = questionService.getAllQuestions();
			adminService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.ADMIN);
    	} catch (Exception e) {
    		e.printStackTrace();
		}
    }

    /** Initialize Method <br>
	 * this will initialize the Question View page<br>
	 * First Question of the Question list is loaded to the view
	 * @param location URL for the Location
	 * @param resources Resource Bundle
	 * */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setQuestion(true);
		btnPrevious.setText("Back");
	}
	
	/** Dynamically View is Loaded on Next and Previous Button Clicks
	 * @param next Is next Button Clicked
	 *  */ 
	private void setQuestion(boolean next) {
		if (questionsList.size() > count) {
			question = questionsList.get(count);
			txtQuestion.setText(question.getQuestion());
			txtQuestion.setPromptText("Question ".concat(String.valueOf(count+1)));
			if (null != question.getAnswer1() && !question.getAnswer1().isEmpty()) {
				chkAnswer1.setText(question.getAnswer1());
				chkAnswer1.setVisible(true);
			} else {
				chkAnswer1.setVisible(false);
			}
			if (null != question.getAnswer2() && !question.getAnswer2().isEmpty()) {
				chkAnswer2.setText(question.getAnswer2());
				chkAnswer2.setVisible(true);
			} else {
				chkAnswer2.setVisible(false);
			}
			if (null != question.getAnswer3() && !question.getAnswer3().isEmpty()) {
				chkAnswer3.setText(question.getAnswer3());
				chkAnswer3.setVisible(true);
			} else {
				chkAnswer3.setVisible(false);
			}
			if (null != question.getAnswer4() && !question.getAnswer4().isEmpty()) {
				chkAnswer4.setText(question.getAnswer4());
				chkAnswer4.setVisible(true);
			} else {
				chkAnswer4.setVisible(false);
			}
			if (null != question.getAnswer5() && !question.getAnswer5().isEmpty()) {
				chkAnswer5.setText(question.getAnswer5());
				chkAnswer5.setVisible(true);
			} else {
				chkAnswer5.setVisible(false);
			}
			chkAnswer1.setSelected(false);
			chkAnswer2.setSelected(false);
			chkAnswer3.setSelected(false);
			chkAnswer4.setSelected(false);
			chkAnswer5.setSelected(false);
			if (next) {//Next Button Click
				questionId = question.getId();
				if (answers.size()>count) {
					questionId = answers.get(count).getQuestionId();
					int answer = answers.get(count).getAnswer();
					if (answer == 1) {
						chkAnswer1.setSelected(true);
						chkAnswer2.setSelected(false);
			    		chkAnswer3.setSelected(false);
			    		chkAnswer4.setSelected(false);
			    		chkAnswer5.setSelected(false);
					} else if (answer == 2) {
						chkAnswer2.setSelected(true);
						chkAnswer5.setSelected(false);
			    		chkAnswer3.setSelected(false);
			    		chkAnswer4.setSelected(false);
			    		chkAnswer1.setSelected(false);
					} else if (answer == 3) {
						chkAnswer3.setSelected(true);
						chkAnswer2.setSelected(false);
			    		chkAnswer5.setSelected(false);
			    		chkAnswer4.setSelected(false);
			    		chkAnswer1.setSelected(false);
					} else if (answer == 4) {
						chkAnswer4.setSelected(true);
						chkAnswer2.setSelected(false);
			    		chkAnswer3.setSelected(false);
			    		chkAnswer5.setSelected(false);
			    		chkAnswer1.setSelected(false);
					} else if (answer == 5) {
						chkAnswer5.setSelected(true);
						chkAnswer2.setSelected(false);
			    		chkAnswer3.setSelected(false);
			    		chkAnswer4.setSelected(false);
			    		chkAnswer1.setSelected(false);
					}
				}
			} else {//Previous Button Click
				questionId = answers.get(count).getQuestionId();
				int answer = answers.get(count).getAnswer();
				if (answer == 1) {
					chkAnswer1.setSelected(true);
					chkAnswer2.setSelected(false);
		    		chkAnswer3.setSelected(false);
		    		chkAnswer4.setSelected(false);
		    		chkAnswer5.setSelected(false);
				} else if (answer == 2) {
					chkAnswer2.setSelected(true);
					chkAnswer5.setSelected(false);
		    		chkAnswer3.setSelected(false);
		    		chkAnswer4.setSelected(false);
		    		chkAnswer1.setSelected(false);
				} else if (answer == 3) {
					chkAnswer3.setSelected(true);
					chkAnswer2.setSelected(false);
		    		chkAnswer5.setSelected(false);
		    		chkAnswer4.setSelected(false);
		    		chkAnswer1.setSelected(false);
				} else if (answer == 4) {
					chkAnswer4.setSelected(true);
					chkAnswer2.setSelected(false);
		    		chkAnswer3.setSelected(false);
		    		chkAnswer5.setSelected(false);
		    		chkAnswer1.setSelected(false);
				} else if (answer == 5) {
					chkAnswer5.setSelected(true);
					chkAnswer2.setSelected(false);
		    		chkAnswer3.setSelected(false);
		    		chkAnswer4.setSelected(false);
		    		chkAnswer1.setSelected(false);
				}
			}
		} else {			
			try {
				ResultDto dto = new ResultDto();
				dto.setUserId(userId);
				dto.setAnswers(answers);
				submissionService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.SUBMISSION);
				boolean isSubmitted = submissionService.submitAnswers(dto);
				
				FinalPageController.userName = userName;
				lblLabel.setText("Thank You");
				AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/finalPage.fxml"));
				btnNext.setVisible(false);
				btnPrevious.setVisible(false);
				if (!innerPane.getChildren().isEmpty()) {
					innerPane.getChildren().clear();
					innerPane.getChildren().add(pane);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
