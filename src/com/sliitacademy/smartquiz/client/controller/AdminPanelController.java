package com.sliitacademy.smartquiz.client.controller;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.beans.Transient;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.custom.AdminService;
import com.sliitacademy.smartquiz.common.service.custom.CustomerService;
import com.sliitacademy.smartquiz.common.service.custom.QuestionService;
import com.sliitacademy.smartquiz.common.service.custom.SubmissionService;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * <h2>Admin Panel Functions are implemented here</h2>
 */
public class AdminPanelController implements Initializable, Serializable {
	
	private static final long serialVersionUID = -3544569994147244856L;

	/** Image View */
	@FXML
    private ImageView imageLoader;

	/** Signed Users Label */
    @FXML
    private Label lblSignedUsers;

	/** Question Count Label */
    @FXML
    private Label lblQuestions;

	/** Manage Users Button */
    @FXML
    private JFXButton btnManageUsers;

	/** Manage Questions Button */
    @FXML
    private JFXButton btnManageQuestions;
    
	/** Next Button for the Graph */
    @FXML
    private JFXButton btnNext;

	/** Previous Button for the Graph */
    @FXML
    private JFXButton btnPrevious;

	/** User Label */
    @FXML
    private Label lblUser;
    
	/** Logout Button */
    @FXML
    private JFXButton btnLogout;
    
	/** Question Summary Text Area */
    @FXML
    private JFXTextArea txtSummary;
    
	/** Customer Service Instance */
    private CustomerService customerService;
    
	/** Question Service Instance */
    private QuestionService questionService;
    
	/** Submission Service Instance */
    private SubmissionService submissionService;
    
	/** Admin Service Instance */
    private AdminService adminService;
    
	/** No of Questions */
    int questionCount = 0;
    
	/** No of Users */
    int userCount = 0;
    
	/** Question Id */
    int questionId = 1;
    
	/** Cookie is Stored Here */
    static String cookie;
    
	/** Logout function for Administrator
	 * This method will navigate to the main page
	 *  */
    @FXML
    public void logOut() {
    	try {
    		Stage primaryStage = (Stage)btnLogout.getScene().getWindow();
			primaryStage.close();
			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/mainView.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
	/** 
	 * View all questions
	 * This method will display all the questions available in the system 
	 * Checks the Session for Expiry and Logs out the user if Session is Expired
	 * @param event Action event of Manage Questions Button
	 * */
    @FXML
    public void manageQuestions(ActionEvent event) {
    	try {
    		if(adminService.isSessionExpired(cookie)) {
    			Alert alert1 = new Alert(AlertType.WARNING);
        		alert1.setContentText("Your Session has been Expired");
        		Optional<javafx.scene.control.ButtonType> result = alert1.showAndWait();
        		if (result.get() == javafx.scene.control.ButtonType.OK){
        			logOut();
        		}
    		} else {
    			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/questionSummary.fxml"));
    			Stage stage = new Stage();
    			Scene scene = new Scene(root);
    			stage.setScene(scene);
    			stage.show();
    		}
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }

    /** 
	 * View all users
	 * This method will display all the users signed in to the system 
	 * Checks the Session for Expiry and Logs out the user if Session is Expired
	 * @param event Action Event of Manage Users Button
	 * */
    @FXML
    public void manageUsers(ActionEvent event) {
    	try {
    		if (adminService.isSessionExpired(cookie)) {
    			Alert alert1 = new Alert(AlertType.WARNING);
        		alert1.setContentText("Your Session has been Expired");
        		Optional<javafx.scene.control.ButtonType> result = alert1.showAndWait();
        		if (result.get() == javafx.scene.control.ButtonType.OK){
        			logOut();
        		}
    		} else {
    			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/userSummary.fxml"));
				Stage stage = new Stage();
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
    		}
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
	/** Navigation option for the Graph
	 * Navigate to the next question summary in Graph view
	 * Checks the Session for Expiry and Logs out the user if Session is Expired
	 * @param event Action Event of Next Button for the Graph
	 *  */
    @FXML
    public void nextSummary(ActionEvent event) {
    	questionId++;
    	ImageIcon image;
		try {
			if (adminService.isSessionExpired(cookie)) {
				Alert alert1 = new Alert(AlertType.WARNING);
        		alert1.setContentText("Your Session has been Expired");
        		Optional<javafx.scene.control.ButtonType> result = alert1.showAndWait();
        		if (result.get() == javafx.scene.control.ButtonType.OK){
        			logOut();
        		}
			} else {
				image = submissionService.getPlottedGraph(questionId);
				QuestionDto dto = questionService.getQuestion(questionId);
				if (null != dto) {
					setQuestion(dto);
					BufferedImage bimage = new BufferedImage(image.getImage().getWidth(null), image.getImage().getHeight(null), BufferedImage.TYPE_INT_RGB);
					Graphics2D bGr = bimage.createGraphics();
					bGr.drawImage(image.getImage(), 0, 0, null);
					bGr.dispose();
					javafx.scene.image.Image image2 = SwingFXUtils.toFXImage(bimage, null);
		    		imageLoader.setImage(image2);
				} else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setContentText("End of Reports");
					alert.show();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/** Navigation option for the Graph
	 * Navigate to the previous question summary in Graph view
	 * Checks the Session for Expiry and Logs out the user if Session is Expired
	 * @param event Action Event of the Previous Button for the Graph
	 *  */
    @FXML
    public void previousSummary(ActionEvent event) {
    	if (questionId != 1) {
    		questionId--;
    	}
    	ImageIcon image;
		try {
			if (adminService.isSessionExpired(cookie)) {
				Alert alert1 = new Alert(AlertType.WARNING);
        		alert1.setContentText("Your Session has been Expired");
        		Optional<javafx.scene.control.ButtonType> result = alert1.showAndWait();
        		if (result.get() == javafx.scene.control.ButtonType.OK){
        			logOut();
        		}
			} else {
				image = submissionService.getPlottedGraph(questionId);
				QuestionDto dto = questionService.getQuestion(questionId);
				setQuestion(dto);
				BufferedImage bimage = new BufferedImage(image.getImage().getWidth(null), image.getImage().getHeight(null), BufferedImage.TYPE_INT_RGB);
				Graphics2D bGr = bimage.createGraphics();
				bGr.drawImage(image.getImage(), 0, 0, null);
				bGr.dispose();
				javafx.scene.image.Image image2 = SwingFXUtils.toFXImage(bimage, null);
				imageLoader.setImage(image2);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	/** Constructor 
	 * Initialization of the services and getting questions and user count is done here
	 * */
    public AdminPanelController() {
		try {
			questionService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.QUESTION);
			customerService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.CUSTOMER);
			submissionService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.SUBMISSION);
			adminService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.ADMIN);
			questionCount = questionService.getQuestionCount();
			userCount = customerService.getUserCount();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** Initialize Method 
	 * this will initialize the Admin Panel page
	 * Initial values are loaded to the view
	 * @param location URL for the Location
	 * @param resources Resource Bundle
	 * */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lblQuestions.setText(String.valueOf(questionCount));
		lblSignedUsers.setText(String.valueOf(userCount));
		txtSummary.setEditable(false);
		try {
			ImageIcon image = submissionService.getPlottedGraph(questionId);
			QuestionDto dto = questionService.getQuestion(questionId);
			if (null != dto) {
				setQuestion(dto);
				BufferedImage bimage = new BufferedImage(image.getImage().getWidth(null), image.getImage().getHeight(null), BufferedImage.TYPE_INT_RGB);
				Graphics2D bGr = bimage.createGraphics();
				bGr.drawImage(image.getImage(), 0, 0, null);
				bGr.dispose();
				javafx.scene.image.Image image2 = SwingFXUtils.toFXImage(bimage, null);
				imageLoader.setImage(image2);
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("No Reports");
				alert.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** This method is Generating a Formatted String with question 
	 * details to be displayed for Each Question in Graph
	 * @param questionDto This Object contains question details
	 * */
	private void setQuestion(QuestionDto dto) {
		StringBuilder text = new StringBuilder();
		text.append(dto.getId() + ") " + dto.getQuestion());
		text.append("\n");
		if (null != dto.getAnswer1() && !dto.getAnswer1().isEmpty())
			text.append("i) " + dto.getAnswer1());
			text.append("\n");
		if (null != dto.getAnswer2() && !dto.getAnswer2().isEmpty())
			text.append("ii) " + dto.getAnswer2());
			text.append("\n");
		if (null != dto.getAnswer3() && !dto.getAnswer3().isEmpty())
			text.append("iii) " + dto.getAnswer3());
			text.append("\n");
		if (null != dto.getAnswer4() && !dto.getAnswer4().isEmpty())
			text.append("iv) " + dto.getAnswer4());
			text.append("\n");
		if (null != dto.getAnswer5() && !dto.getAnswer5().isEmpty())
			text.append("v) " + dto.getAnswer5());
			txtSummary.setText(text.toString());
	}
}
