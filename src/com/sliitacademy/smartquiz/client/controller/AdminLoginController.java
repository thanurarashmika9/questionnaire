package com.sliitacademy.smartquiz.client.controller;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.sliitacademy.smartquiz.client.dto.AdminLoginDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.custom.AdminService;
import com.sliitacademy.smartquiz.common.service.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * <h2>Admin Login panel functions are implemented here</h2>
 * */

public class AdminLoginController {
	/**
	 *User name text field 
	 * */
	@FXML
	private JFXTextField txtUsername;
	
	/** Password text field */
	@FXML
	private JFXPasswordField txtPassword;
	
	/** Login button */
	@FXML
	private JFXButton btnLogin;
	
	/** Cancel Button */
	@FXML
	private JFXButton btnCancel;
	
	/** Constructor */
	public AdminLoginController() {
		
	}
	
	/** Admin Service Connection with the Controller */
	private AdminService adminService;
	
	/** 
	 * Action for login button click event<br>
	 * Administrator login function is implemented here<br>
	 * Username and Password are Passed to the Server and a Cookie will be received from the
	 * Server on Successful Login. Then the Cookie is maintained in the client and will be used to Manage the Session.<br>
	 * if Username and Password are Matching with the Admin Credentials, Admin Panel is loaded. Else an Error Message will be Prompted
	*/
	@FXML
	public void login() {
		try {
			adminService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.ADMIN);
			AdminLoginDto adminLoginDto = new AdminLoginDto();
			adminLoginDto.setUserName(txtUsername.getText());
			adminLoginDto.setPassword(txtPassword.getText());
			String cookie = adminService.login(adminLoginDto);
			if (null != cookie && !cookie.isEmpty()) {
				AdminPanelController.cookie = cookie;
				Stage primaryStage = (Stage)btnCancel.getScene().getWindow();
				primaryStage.close();
				Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/adminPanel.fxml"));
				Stage stage = new Stage();
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Invalid Credentials");
				alert.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	 * Action for cancel button click event in login screen
	 * This method is Navigates the user to main page from login page
	*/
	@FXML
	public void cancel() {
		try {
			Stage primaryStage = (Stage)btnCancel.getScene().getWindow();
			primaryStage.close();
			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/mainView.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
