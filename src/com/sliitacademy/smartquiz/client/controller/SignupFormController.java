package com.sliitacademy.smartquiz.client.controller;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.sliitacademy.smartquiz.client.dto.SignupDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.ServiceFactory.ServiceTypes;
import com.sliitacademy.smartquiz.common.service.custom.CustomerService;
import com.sliitacademy.smartquiz.common.service.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * <h2>Signup Page Functions are implemented here</h2>
 */
public class SignupFormController {

	/** Cance Button */
    @FXML
    private JFXButton btnCancel;

	/** Take Quiz Button */
    @FXML
    private JFXButton btnTakeQuiz;

	/** Text Field to input User's name */
    @FXML
    private JFXTextField txtName;

	/** Text Field to input Age */
    @FXML
    private JFXTextField txtAge;

	/** Textfield to input Contact Number */
    @FXML
    private JFXTextField txtContact;

	/** Instance of Customer Service */
    private CustomerService customerService; 
    
	/** Default Constructor */
    public SignupFormController() {
	}

	/** Action Event for Cancel Button click */
    @FXML
    void doCancel() {
    	try {
    		Stage primaryStage = (Stage)btnCancel.getScene().getWindow();
			primaryStage.close();
			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/mainView.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
    	} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/** Action Event for Take Quiz Button Click */
    @FXML
    void takeQuiz() {
    	try {
    		SignupDto signupDto = new SignupDto();
    		if (txtName.getText().isEmpty() || txtAge.getText().isEmpty() || txtContact.getText().isEmpty()) {
    			Alert alert = new Alert(AlertType.WARNING);
    			alert.setContentText("Please Provide All Details");
    			alert.show();
    		} else if (txtAge.getText().matches("[0-9]+") && txtContact.getText().matches("[0-9]+")) {
    			customerService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.CUSTOMER);
    			signupDto.setName(txtName.getText());
    			signupDto.setAge(Integer.parseInt(txtAge.getText()));
    			signupDto.setContact(Integer.parseInt(txtContact.getText()));
    			QuestionViewController.userName = txtName.getText();
    			String cookie = customerService.signup(signupDto);
    			QuestionViewController.userId = Integer.parseInt(cookie);
    			Stage primaryStage = (Stage)btnCancel.getScene().getWindow();
    			primaryStage.close();
				Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/questionView.fxml"));
				Stage stage = new Stage();
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
    		} else {
    			Alert alert = new Alert(AlertType.WARNING);
    			alert.setContentText("Please Provide Valid Details");
    			alert.show();
    		}
    	} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
