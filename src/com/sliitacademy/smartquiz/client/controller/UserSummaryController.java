package com.sliitacademy.smartquiz.client.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.sliitacademy.smartquiz.client.dto.SignupDto;
import com.sliitacademy.smartquiz.client.service.ServiceConnector;
import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.custom.CustomerService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * <h2>User Summary Page Functions are implemented here</h2>
 */
public class UserSummaryController implements Initializable {
	
	/** Table view to load User details */
	@FXML
    private TableView<SignupDto> tblUserView;
	
	/** List which stores the User details */
	private List<SignupDto> userList = null;
	
	/** Observable list which is loaded to the Table */
	private ObservableList<SignupDto> tvObservableList = null;
	
	/** Instance of Customer Service */
	private CustomerService customerService;

	/** Initialize Method <br>
	 * this will initialize the User Summary page<br>
	 * All the User details will be loaded to the Table view
	 * @param location URL for the Location
	 * @param resources Resource Bundle
	 * */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		getAllUsers();
        tblUserView.setItems(tvObservableList);

        tblUserView.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        tblUserView.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblUserView.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("age"));
        tblUserView.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("contact"));
        tblUserView.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("date"));
	}
	
	/** All User details are accessed from the server through this method and arranged them in the observable list
	 * so that it is applicable in the table view*/
	private void getAllUsers() {
		try {
			customerService = ServiceConnector.getInstance().getService(ServiceFactory.ServiceTypes.CUSTOMER);
			userList = customerService.getAllUsers();
			tvObservableList = FXCollections.observableArrayList(userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
