package com.sliitacademy.smartquiz.client.controller;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * <h2>Main Page Functions are implemented here</h2>
 */
public class MainViewController {
	/** Main Pane */
	@FXML
	private AnchorPane rootPane;
	
	/** Signup Button */
	@FXML
	private JFXButton btnSignup;
	
	/** Administrator Login Button */
	@FXML
	private JFXButton btnLogin;
	
	@FXML
	private Label lblTest;
	
	/** Default Constructor */
	public MainViewController() {
		
	}
	
	/** Action for Signup button click event 
	 * User will be redirected to the Signup Page
	 * */
	@FXML
	public void doSignup() {
		try {
			AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/singupForm.fxml"));
			if (!rootPane.getChildren().isEmpty())
				rootPane.getChildren().clear();
				this.rootPane.getChildren().add(pane);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Action for Administrator Login button click event
	 * User will be redirected to the Admin Panel Login Page */
	@FXML
	public void doLogin() {
		try {
			AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/adminLogin.fxml"));
			if (!rootPane.getChildren().isEmpty())
				rootPane.getChildren().clear();
				this.rootPane.getChildren().add(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
