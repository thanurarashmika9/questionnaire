package com.sliitacademy.smartquiz.client.dto;

import java.io.Serializable;

/**
 * QuestionDto is the Object used to Transfer Questions between client and Server
 */
public class QuestionDto implements Serializable {
	private static final long serialVersionUID = -6526295352621803176L;
	
	/** Question ID */
	private int id;
	/** Question Description */
	private String question;
	/**First Answer of the Question*/
	private String answer1;
	/**Second Answer of the Question*/
	private String answer2;
	/**Third Answer of the Question*/
	private String answer3;
	/**Fourth Answer of the Question*/
	private String answer4;
	/**Fifth Answer of the Question*/
	private String answer5;
	
	public QuestionDto() {}
	
	/** get the Question Description
	 * @return question Description
	 * */
	public String getQuestion() {
		return question;
	}

	/** set Question Description 
	 * @param question Question Description
	 * */
	public void setQuestion(String question) {
		this.question = question;
	}

	/** get First Answer 
	 * @return First Answer
	 * */
	public String getAnswer1() {
		return answer1;
	}

	/** set First Answer 
	 * @param answer1 First Answer
	 * */
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	/** get Second Answer 
	 * @return Second Answer
	 * */
	public String getAnswer2() {
		return answer2;
	}

	/** set Second Answer 
	 * @param answer2 Second Answer
	 * */
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	/** get Third Answer 
	 * @return Third Answer
	 * */
	public String getAnswer3() {
		return answer3;
	}

	/** set Third Answer 
	 * @param answer3 Third Answer
	 * */
	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	/** get Fourth Answer 
	 * @return Fourth Answer
	 * */
	public String getAnswer4() {
		return answer4;
	}

	/** set Fourth Answer 
	 * @param answer4 Fourth Answer
	 * */
	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}

	/** get Fifth Answer 
	 * @return Fifth Answer
	 * */
	public String getAnswer5() {
		return answer5;
	}

	/** set Fifth Answer 
	 * @param answer5 Fifth Answer
	 * */
	public void setAnswer5(String answer5) {
		this.answer5 = answer5;
	}

	/** get Question Id
	 * @return Question Id
	 * */
	public int getId() {
		return id;
	}

	/** set Question Id
	 * @param id Question Id
	 * */
	public void setId(int id) {
		this.id = id;
	}
}
