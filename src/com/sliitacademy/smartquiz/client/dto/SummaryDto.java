package com.sliitacademy.smartquiz.client.dto;

/**SummaryDto is Used to handle Question wise User Summary*/
public class SummaryDto {
	/** Question Id */
	private int qid;
	/** Answer Id */
	private int ansId;
	/** Users Count who gave a Specific Answer */
	private int users;
	
	/** get Question Id 
	 * @return Question Id
	 * */
	public int getQid() {
		return qid;
	}
	
	/** set Question Id 
	 * @param qid Question Id
	 * */
	public void setQid(int qid) {
		this.qid = qid;
	}
	
	/** get Users Count answered Specific Question 
	 * @return Users Count
	 * */ 
	public String getUsers() {
		return Integer.toString(users);
	}
	
	/** set Users Count answered Specific Answer 
	 * @param users Users Count
	 * */
	public void setUsers(int users) {
		this.users = users;
	}
	
	/** get Answer Id 
	 * @return Id of the Answer
	 * */
	public String getAnsId() {
		return Integer.toString(ansId);
	}
	
	/** set Answer Id 
	 * @param ansId Answer Id
	 * */
	public void setAnsId(int ansId) {
		this.ansId = ansId;
	}
}
