package com.sliitacademy.smartquiz.client.dto;

import java.io.Serializable;
/**
 * The AnswerDto Class is used to transfer Answers
 * from Client to Server
 */

public class AnswerDto implements Serializable {
	private static final long serialVersionUID = 6919706928272025995L;
	
	/** Question ID */
	private Integer questionId;
	
	/** Answer Number*/
	private Integer answer;
	
	/** get Question Id 
	 * @return Question Id
	 * */
	public Integer getQuestionId() {
		return questionId;
	}
	
	/** set Question Id 
	 * @param questionId Id of the Question
	 * */
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	/** get answer 
	 * @return Answer Number
	 * */
	public Integer getAnswer() {
		return answer;
	}
	
	/** set answer 
	 * @param answer Answer Number
	 * */
	public void setAnswer(Integer answer) {
		this.answer = answer;
	}
	
	
}
