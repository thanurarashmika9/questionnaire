package com.sliitacademy.smartquiz.client.dto;

import java.io.Serializable;
import java.math.BigInteger;

/**SignupDto handles User data which he/ she enters in the Signup page*/
public class SignupDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6064351379380335219L;
	
	/** Id Generated for the User */
	private int id;
	
	/** Name of the User */
	private String name;
	
	/** Age of the User */
	private int age;
	
	/** Contact Number of the User */
	private int contact;
	
	/** Date of Signup */
	private String date;
	
	/** get User Id 
	 * @return User Id
	 * */
	public int getId() {
		return id;
	}
	
	/** set User Id
	 * @param id Generated User Id
	 *  */
	public void setId(int id) {
		this.id = id;
	}
	
	/** get User Name 
	 * @return Name of the User
	 * */
	public String getName() {
		return name;
	}
	
	/** set Name of the User 
	 * @param name Name of the User
	 * */
	public void setName(String name) {
		this.name = name;
	}
	
	/** get Age of the User 
	 * @return Age of the User
	 * */
	public int getAge() {
		return age;
	}
	
	/** set Age 
	 * @param age User's Age
	 * */
	public void setAge(int age) {
		this.age = age;
	}
	
	/** get Contact Number 
	 * @return Contact Number of the User
	 * */
	public int getContact() {
		return contact;
	}
	
	/** set Contact Number 
	 * @param contact User's Contact Number
	 * */
	public void setContact(int contact) {
		this.contact = contact;
	}
	
	/** get Signin Date 
	 * @return Signin Date and Time
	 * */
	public String getDate() {
		return date;
	}
	
	/** set Signin Date 
	 * @param date Date and Time of Signin
	 * */
	public void setDate(String date) {
		this.date = date;
	}
}
