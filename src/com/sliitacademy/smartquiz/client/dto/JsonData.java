package com.sliitacademy.smartquiz.client.dto;

import java.io.Serializable;
import java.util.List;

/**
 *The JsonData Class is used to transfer Report details
 *From server to client
 */

public class JsonData implements Serializable {

	private static final long serialVersionUID = -3114754364504172653L;

	/** Question ID */
	private int questionId;
	/** Description of the Question */
	private String question;
	/** Answer 1 for the Question */
	private String answer1;
	/** Answer 2 for the Question */
	private String answer2;
	/** Answer 3 for the Question */
	private String answer3;
	/** Answer 4 for the Question */
	private String answer4;
	/** Answer 5 for the Question */
	private String answer5;
	/** List of Question wise User Summary */
	private List<SummaryDto> users; 
	
	/** get Question ID 
	 * @return Question Id*/
	public int getQuestionId() {
		return questionId;
	}
	/**
	 * set Question ID
	 * @param questionId Question Id
	 */
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	/** get Question Description 
	 * @return Description of the Question
	 * */
	public String getQuestion() {
		return question;
	}
	/** set Question Description 
	 * @param question Question Description
	 * */
	public void setQuestion(String question) {
		this.question = question;
	}
	
	/** get First Answer 
	 * @return First Answer
	 * */
	public String getAnswer1() {
		return answer1;
	}
	/** set First Answer 
	 * @param answer1 First Answer
	 * */
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}
	
	/** get Second Answer 
	 * @return Second Answer
	 * */
	public String getAnswer2() {
		return answer2;
	}
	/** set Second Answer 
	 * @param answer2 Second Answer
	 * */
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}
	
	/** get Third Answer 
	 * @return Third Answer
	 * */
	public String getAnswer3() {
		return answer3;
	}
	/** set Third Answer 
	 * @param answer3 Third Answer
	 * */
	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}
	
	/** get Fourth Answer 
	 * @return Fourth Answer
	 * */
	public String getAnswer4() {
		return answer4;
	}
	/** set Fourth Answer 
	 * @param answer4 Fourth Answer
	 * */
	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}
	
	/** get First Answer 
	 * @return First Answer
	 * */
	public String getAnswer5() {
		return answer5;
	}
	/** set Fifth Answer 
	 * @param answer5 Fifth Answer
	 * */
	public void setAnswer5(String answer5) {
		this.answer5 = answer5;
	}

	/** get User wise Answer Summary 
	 * @return List of Summarized data about Users
	 * who answered the questions 
	 * */
	public List<SummaryDto> getUsers() {
		return users;
	}
	/** set User wise Answer Summary 
	 * @param users List of Summarized data about Users
	 * who answered the questions
	 * */
	public void setUsers(List<SummaryDto> users) {
		this.users = users;
	}
}
