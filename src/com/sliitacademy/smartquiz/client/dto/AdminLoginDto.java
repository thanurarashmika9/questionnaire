package com.sliitacademy.smartquiz.client.dto;

import java.io.Serializable;
/**
 * The AdminLoginDto Class is used to transfer login credentials
 * between client and server
 */

public class AdminLoginDto implements Serializable {
	
	/**
	 *Serialized id 
	 **/
	private static final long serialVersionUID = 822771734291639498L;
	
	/** ID of the user */
	private int id;
	
	/** Username of the user */
	private String userName;
	
	/** Password of the user */
	private String password;
	
	
	/** Default Constructor */
	public AdminLoginDto() {
		
	}

	/** get the User ID 
	 * @return User Id
	 * */
	public int getId() {
		return id;
	}
	
	/** set the User ID 
	 * @param id User Id
	 * */
	public void setId(int id) {
		this.id = id;
	}
	
	/** get the User name 
	 * @return User Name
	 * */
	public String getUserName() {
		return userName;
	}

	/** set the User Name 
	 * @param userName User Name
	 * */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/** get the User Password 
	 * @return User Password
	 * */
	public String getPassword() {
		return password;
	}

	/** set the User Password 
	 * @param password User Password
	 * */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
