package com.sliitacademy.smartquiz.client.dto;

import java.io.Serializable;
import java.util.List;

/**ResultDto Class is to handle responses provided by the 
 * users to the questions and transport them to the server
 * */
public class ResultDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1881173775329831386L;
	
	/** User Id */
	private int userId;
	
	/** Answers provided by the user during the questionnaire */
	private List<AnswerDto> answers;
	
	/** getget User Id 
	 * @return User Id
	 * */
	public int getUserId() {
		return userId;
	}
	
	/** set User Id 
	 * @param userName Name of the User
	 * */
	public void setUserId(int userName) {
		this.userId = userName;
	}
	
	/** get the Answers provided by the user
	 * @return List of Answers relevant to the Question
	 * */
	public List<AnswerDto> getAnswers() {
		return answers;
	}
	
	/** set the Answers 
	 * @param answers List of Answers for Each Question
	 * */
	public void setAnswers(List<AnswerDto> answers) {
		this.answers = answers;
	}
	
	
}
