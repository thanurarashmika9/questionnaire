package com.sliitacademy.smartquiz.client;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * @author Thanura Rashmika
 * Client Application Start
*/

public class Main extends Application {
	/** Client side Main Method to start the Application
	 * Application is in JavaFX
	 * @param primaryStage Main View
	 * */
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(this.getClass().getResource("/com/sliitacademy/smartquiz/client/view/mainView.fxml"));
			Scene mainScene = new Scene(root);
			primaryStage.setScene(mainScene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
