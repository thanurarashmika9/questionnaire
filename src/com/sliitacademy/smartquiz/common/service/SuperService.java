package com.sliitacademy.smartquiz.common.service;

import java.rmi.Remote;

/**All Services are Children of this Super Service Interface<br>
 * All Services have the properties of this Interface*/
public interface SuperService extends Remote {

}
