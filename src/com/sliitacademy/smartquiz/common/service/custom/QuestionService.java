package com.sliitacademy.smartquiz.common.service.custom;

import java.util.List;

import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.common.service.SuperService;

/**Admin Service Interface<br>
 * Admin Related Functions are defined here and implemented in the child class
 * */
public interface QuestionService extends SuperService {
	
	/** Save new Question function 
	 * @param questionDto Question Details
	 * @return Whether Saved or not
	 * @throws Exception
	 * */
	public boolean saveQuestion(QuestionDto questionDto) throws Exception;
	
	/** Update Question Function 
	 * @param questionDto Question Details
	 * @return Whether Updated or not
	 * @throws Exception
	 * */
	public boolean updateQuestion(QuestionDto questionDto) throws Exception;
	
	
	/** Delete Question Function 
	 * @param questionId Question Id
	 * @return Whether Deleted or not
	 * @throws Exception
	 * */
	public boolean deleteQuestion(int questionId) throws Exception;
	
	/** Get All Questions Function 
	 * @return List of Question Details
	 * @throws Exception
	 * */
	public List<QuestionDto> getAllQuestions() throws Exception;
	
	/** Get Question Count Function 
	 * @return Number of Questions available in the System
	 * @throws Exception
	 * */
	public Integer getQuestionCount() throws Exception;

	/** Get a Question Function 
	 * @param questionId Id of the Specific Question
	 * @return Question Details
	 * @throws Exception
	 * */
	public QuestionDto getQuestion(int questionId) throws Exception;
}
