package com.sliitacademy.smartquiz.common.service.custom;

import com.sliitacademy.smartquiz.client.dto.AdminLoginDto;
import com.sliitacademy.smartquiz.common.service.SuperService;

/**Admin Service Interface<br>
 * Admin Related Functions are defined here and implemented in the child class
 * */
public interface AdminService extends SuperService {
	
	/** Login Function 
	 * @param adminLoginDto
	 * @return Session Cookie
	 * @throws Exception
	 * */
	public String login(AdminLoginDto adminLoginDto) throws Exception;
	
	/** Checks the Session Expiry 
	 * @param cookie
	 * @return If Session is Expired
	 * @throws Exception
	 * */
	public boolean isSessionExpired(String cookie) throws Exception;
}
