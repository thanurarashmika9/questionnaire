package com.sliitacademy.smartquiz.common.service.custom;

import javax.swing.ImageIcon;

import com.sliitacademy.smartquiz.client.dto.ResultDto;
import com.sliitacademy.smartquiz.common.service.SuperService;

/**Submission Service Interface<br>
 * Submission Related Functions are defined here and implemented in the child class
 * */
public interface SubmissionService extends SuperService {
	
	/** Submit Answers for a Question
	 * @param results Submission Details including all QUestions and their Answers
	 * @return If the Responses for the Questions Saved or not
	 * @throws Exception*/
	public boolean submitAnswers(ResultDto results) throws Exception;
	
	/** Graphical view of the Submissions 
	 * @param questionId Id of the Question
	 * @return Image of the Graphical representation of the Number of users vs Answer for each Question
	 * @throws Exception
	 * */
	public ImageIcon getPlottedGraph(int questionId) throws Exception;
}
