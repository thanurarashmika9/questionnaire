package com.sliitacademy.smartquiz.common.service.custom;

import java.util.List;

import com.sliitacademy.smartquiz.client.dto.SignupDto;
import com.sliitacademy.smartquiz.common.service.SuperService;

/**Customer Service Interface<br>
 * Customer Related Functions are defined here and implemented in the child class
 * */
public interface CustomerService extends SuperService {
	
	/** Signup Function 
	 * @param signupDto Signup Details
	 * @return Session Cookie
	 * @throws Exception
	 * */
	public String signup(SignupDto signupDto) throws Exception;
	
	/** get All User Details 
	 * @return User Details List
	 * @throws Exception
	 * */
	public List<SignupDto> getAllUsers() throws Exception; 
	
	/** get Users Count 
	 * @return Users Count
	 * @throws Exception
	 * */
	public Integer getUserCount() throws Exception;
}
