package com.sliitacademy.smartquiz.common.service;

import java.rmi.Remote;

/**Acts as a Factory which Defines the Service Types*/
public interface ServiceFactory extends Remote {
	
	/** Service Types are Defined as Enum */
	public enum ServiceTypes {
		CUSTOMER, ADMIN, QUESTION, SUBMISSION;
	}
	
	/** Can access a Service type Enum with this method 
	 * @param serviceTypes Enum for Service Type
	 * @param ServiceTypes
	 * @return Service Type
	 * @throws Exception
	 * */
	public <T extends SuperService>T getService(ServiceTypes serviceTypes) throws Exception;
}