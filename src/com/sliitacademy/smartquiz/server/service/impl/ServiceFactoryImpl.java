package com.sliitacademy.smartquiz.server.service.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.sliitacademy.smartquiz.common.service.ServiceFactory;
import com.sliitacademy.smartquiz.common.service.SuperService;
import com.sliitacademy.smartquiz.server.service.custom.impl.AdminServiceImpl;
import com.sliitacademy.smartquiz.server.service.custom.impl.CustomerServiceImpl;
import com.sliitacademy.smartquiz.server.service.custom.impl.QuestionServiceImpl;
import com.sliitacademy.smartquiz.server.service.custom.impl.SubmissionServiceImpl;

/**Implementation of the ServiceFactory<br>
 * Services are managed for the Access from the Client with Singleton Design Pattern*/
public class ServiceFactoryImpl extends UnicastRemoteObject implements ServiceFactory {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9164516783231518324L;

	/** Default Constructor */
	protected ServiceFactoryImpl() throws RemoteException {
		super();
	}
	
	/** Static Instance of the ServiceFactoryImpl */
	private static ServiceFactoryImpl serviceFactory;
	
	/** Creates an Static Instance of Service Factory to Access a Specific Service 
	 * @return serviceFactoryImpl Instance 
	 * @throws Exception
	 * */
	public static ServiceFactoryImpl getInstance() throws Exception {
		if (null == serviceFactory) {
			serviceFactory = new ServiceFactoryImpl();
		}
		return serviceFactory;
	}

	/** Can access a Service Implementation of the required Service
	 * @param serviceTypes Enum for the Service
	 * @return Service Instance for the Enum
	 * @throws Exception
	 *  */
	@Override
	public <T extends SuperService> T getService(ServiceTypes serviceTypes) throws Exception {
		switch(serviceTypes) {
			case CUSTOMER:
				return (T) new CustomerServiceImpl();
			case ADMIN:
				return (T) new AdminServiceImpl();
			case QUESTION:
				return (T) new QuestionServiceImpl();
			case SUBMISSION:
				return (T) new SubmissionServiceImpl();
			default:
				return null;
		}
	}

}
