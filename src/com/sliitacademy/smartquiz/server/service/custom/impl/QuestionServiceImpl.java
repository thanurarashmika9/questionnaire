package com.sliitacademy.smartquiz.server.service.custom.impl;

import java.awt.Image;
import java.net.URL;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.sliitacademy.smartquiz.client.dto.JsonData;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.common.service.custom.QuestionService;
import com.sliitacademy.smartquiz.server.SessionStorage;
import com.sliitacademy.smartquiz.server.dao.custom.QuestionDao;
import com.sliitacademy.smartquiz.server.dao.custom.impl.QuestionDaoImpl;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of QuestionService
 * Questions Related Functions are Implemented here
 * */
public class QuestionServiceImpl extends UnicastRemoteObject implements QuestionService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3105102440355354816L;
	
	/** Instance of {@link QuestionDao} for Database Management */
	private QuestionDao questionDao = new QuestionDaoImpl();
	
	/** Default Constructor 
	 * @throws RemoteException*/
	public QuestionServiceImpl() throws RemoteException {
		super();
	}

	/** Save Question<br> 
	 * New Question is saved through this method. Question Description and the Answers are passed into this method
	 * and then the Session will be checked. If the Session is Active the Question will be Passed to {@link QuestionDao}
	 * to be saved in the Database
	 * @param questionDto Question Details
	 * @return If the Question is Saved or not
	 * */
	@Override
	public boolean saveQuestion(QuestionDto questionDto) throws Exception {
		boolean isSaved = false;
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			if (SessionStorage.checkSession(true)) {
				isSaved = questionDao.save(connection, questionDto) > 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isSaved;
	}

	/** Get All Questions<br> 
	 * Session will be checked. If the Session is Active, 
	 * All the Questions in the System are Returned from this method
	 * @return List of Questions with Question Details
	 * */
	@Override
	public List<QuestionDto> getAllQuestions() throws Exception {
		List<QuestionDto> questions = new ArrayList<>();
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			if (SessionStorage.checkSession(false)) {
				questions = questionDao.getAll(connection);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return questions;
	}

	/** Update Question Function<br> 
	 * Session will be checked. If the Session is Active, Question can be Updated with this Method.
	 *  Question Details are taken as parameters through {@link QuestionDto}
	 * and returned whether updated or not
	 * @param questionDto Question Details
	 * @return If the Quetion is Updated or not
	 * */
	@Override
	public boolean updateQuestion(QuestionDto questionDto) throws Exception {
		boolean isUpdate = false;
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			if (SessionStorage.checkSession(true)) {
				isUpdate = questionDao.updateQuestion(connection, questionDto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isUpdate;
	}

	/** Delete Question Function<br> 
	 * Session will be checked. If the Session is Active, 
	 * Question can be Deleted from the System with this Method
	 * @param questionId Id of the Question to be Deleted
	 * @return If the Question Deleted or not
	 * */
	@Override
	public boolean deleteQuestion(int questionId) throws Exception {
		boolean isDelete = false;
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			if (SessionStorage.checkSession(true)) {
				isDelete = questionDao.deleteQuestion(connection, questionId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isDelete;
	}

	/** Get Questions Count<br> 
	 * Number of Availablle Questions are returned through this method
	 * @return Number of Questions available in the System
	 * */
	@Override
	public Integer getQuestionCount() throws Exception {
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			return questionDao.getQuestionCount(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/** Get a Specific Question <br>
	 * Specific Question can be Searched with this method
	 * Session will be checked. If the Session is Active, the Searched Question Details will be returned
	 * @param questionId Id of the Question
	 * @return Question Details for the Question Id
	 * */
	@Override
	public QuestionDto getQuestion(int questionId) throws Exception {
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			if (SessionStorage.checkSession(false)) {
				return questionDao.getQuestion(connection, questionId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return null;
	}

}
