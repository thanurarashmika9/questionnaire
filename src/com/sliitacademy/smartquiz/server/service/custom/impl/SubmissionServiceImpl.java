package com.sliitacademy.smartquiz.server.service.custom.impl;

import java.net.URL;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.util.List;

import javax.swing.ImageIcon;

import com.sliitacademy.smartquiz.client.dto.AnswerDto;
import com.sliitacademy.smartquiz.client.dto.JsonData;
import com.sliitacademy.smartquiz.client.dto.ResultDto;
import com.sliitacademy.smartquiz.client.dto.SummaryDto;
import com.sliitacademy.smartquiz.common.service.custom.SubmissionService;
import com.sliitacademy.smartquiz.server.dao.custom.CustomerDao;
import com.sliitacademy.smartquiz.server.dao.custom.QuestionDao;
import com.sliitacademy.smartquiz.server.dao.custom.SubmissionDao;
import com.sliitacademy.smartquiz.server.dao.custom.impl.CustomerDaoImpl;
import com.sliitacademy.smartquiz.server.dao.custom.impl.SubmissionDaoImpl;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of SubmissionService
 * Answer Submission Related Functions are Implemented here
 * */
public class SubmissionServiceImpl extends UnicastRemoteObject implements SubmissionService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5852843388827930582L;
	
	/** Instance of {@link SubmissionDao} for Database Management */
	private SubmissionDao submissionDao = new SubmissionDaoImpl();
	
	/** Instance of {@link CustomerDao} for Database Management */
	private CustomerDao customerDao = new CustomerDaoImpl();

	/** Default Constructor 
	 * @throws RemoteException
	 * */
	public SubmissionServiceImpl() throws RemoteException {
		super();
	}

	/** Submit Answers<br>
	 * Submitted Answers are saved through this method
	 * @param result UserId, Question and Answer will be passed through {@link ResultDto} as Parameter
	 * to this method
	 * @return If the Answers Submitted or not
	 *  */
	@Override
	public boolean submitAnswers(ResultDto result) throws Exception {
		boolean isSubmitted = false;
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			for (AnswerDto answer : result.getAnswers()) {
				isSubmitted = submissionDao.submitAnswers(connection, result.getUserId(), answer.getQuestionId(), answer.getAnswer());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isSubmitted;
	}

	/** Get Plotted Graph<br>
	 * @see <a href="https://quickchart.io/">Quick Chart</a> is Used as a third party Service to Draw the Charts.
	 * Here the Question Id is passed and Summarized data is taken from the Database as {@link JsonData} Object.
	 * Then the Data is Processed and a JSON Object is Created and then it is passed to the External API to get an Image of the
	 * Generated Chart
	 * @param questionId Id of the Question
	 * @return Image of the Plotted Graph
	 *  */
	@Override
	public ImageIcon getPlottedGraph(int questionId) throws Exception {
		try {
			String labels = "[" + 2020 + "," + 2021 + "," + 2022 + "," + 2023 + "]";
      	  	String data = "[" + 120 + "," + 130 + "," + 140 + "," + 150 + "]";
            String url = "https://quickchart.io/chart";
            String charset = "UTF-8";
            Connection connection = DBConnection.getInstance().getConnection();
            JsonData json = submissionDao.generateReport(connection, questionId);
            if (null != json.getQuestion() && !json.getQuestion().isEmpty()) {
            	String param1 = getJson(json);

            	String query = String.format("bkg=white&c=%s", 
            			URLEncoder.encode(param1, charset));
            	URL urls = new URL(url + "?" + query);
            	ImageIcon image = new ImageIcon(urls);
            	return image;
            }
            return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	/** JSON Object is Created with this Method for the External API<br>
	 * External API Requests and Responses are available @see <a href="https://quickchart.io/documentation/">Documentation</a>
	 * @param jsonData Summaried Data of a Specific Question
	 * @return JSON Object created as a String
	 *  */
	private String getJson(JsonData jsonData) {
		StringBuilder labels = new StringBuilder();
		StringBuilder data = new StringBuilder();
		List<SummaryDto> summary = jsonData.getUsers();
		for (int i=0; i<summary.size(); i++) {
			if (i == 0) {
				labels.append("[");
				data.append("[");
			}
			labels.append("\'Answer ").append(summary.get(i).getAnsId()).append("\'");
			data.append(summary.get(i).getUsers());
			if (i < (summary.size() - 1)) {
				labels.append(",");
				data.append(",");
			} else {
				labels.append("]");
				data.append("]");
			}
		}
		String json = "{\n"
        		+ "	type: 'bar',\n"
        		+ "	data: {\n"
        		+ "		labels: " + labels.toString() + ",\n"
        		+ "		datasets: [{\n"
        		+ "			label: 'Users',\n"
        		+ "			data: " + data.toString() + "\n"
        		+ "		}]\n"
        		+ "	}\n"
        		+ "}";
		return json;
	}

}
