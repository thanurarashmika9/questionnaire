package com.sliitacademy.smartquiz.server.service.custom.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;

import com.sliitacademy.smartquiz.client.dto.AdminLoginDto;
import com.sliitacademy.smartquiz.common.service.custom.AdminService;
import com.sliitacademy.smartquiz.server.SessionStorage;
import com.sliitacademy.smartquiz.server.dao.custom.AdminDao;
import com.sliitacademy.smartquiz.server.dao.custom.impl.AdminDaoImpl;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of AdminService
 * Admin Related functions are Implemented here
 * */
public class AdminServiceImpl extends UnicastRemoteObject implements AdminService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3163257406323794339L;
	
	/** Instance of {@link AdminDao} for Database Management*/
	private AdminDao adminDao = new AdminDaoImpl();
	
	/** Default Constructor 
	 * @throws RemoteException*/
	public AdminServiceImpl() throws RemoteException {
		super();
	}

	/** Administrator Login Function<br> 
	 * Login Details (Username and Password) are passed to this method as {@link AdminLoginDto} and the Credentials are Checked.
	 *  If the Password match with the Saved Password, then a Cookie is Generated to maintain Session Details.
	 *  A Timer will Start to Maintain the Active Session
	 *  @param adminLoginDto Login Details
	 *  @return Cookie Created for the Session
	 * */
	@Override
	public String login(AdminLoginDto adminLoginDto) throws Exception {
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			AdminLoginDto credentials = adminDao.getLoginCredentials(connection, adminLoginDto.getUserName());
			if (null != credentials && credentials.getPassword().equals(adminLoginDto.getPassword())) {
				String cookie = "ADMIN".concat(adminLoginDto.getUserName()).concat(adminLoginDto.getPassword());
				SessionStorage.cookie = cookie;
				SessionStorage.timeout = System.nanoTime();
				return cookie;
			}
		} catch (Exception e) {
			throw e;
		}
		return null;
	}

	/** Check if the Session is Expired 
	 * Cookie Generated when Login is Passed to this method and the Session is Checked
	 * <b>{@link SessionStorage}</b>
	 * @param cookie Generated Cookie
	 * */
	@Override
	public boolean isSessionExpired(String cookie) throws Exception {
		try {
			return !SessionStorage.checkSession(cookie.startsWith("ADMIN"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
