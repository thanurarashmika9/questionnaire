package com.sliitacademy.smartquiz.server.service.custom.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.sliitacademy.smartquiz.client.dto.SignupDto;
import com.sliitacademy.smartquiz.common.service.custom.CustomerService;
import com.sliitacademy.smartquiz.server.SessionStorage;
import com.sliitacademy.smartquiz.server.dao.custom.CustomerDao;
import com.sliitacademy.smartquiz.server.dao.custom.impl.CustomerDaoImpl;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of CustomerService
 * Customer/ User Related Functions are Implemented here
 * */
public class CustomerServiceImpl extends UnicastRemoteObject implements CustomerService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8613718932407333230L;
	
	/** Instance of @link CustomerDao for Database Management*/
	private CustomerDao customerDao = new CustomerDaoImpl();

	/** Default Constructor 
	 * @throws RemoteException*/
	public CustomerServiceImpl() throws RemoteException {
		super();
	}

	/** User Signup Function
	 * User Details(Name, Age and Contact Number) are passed to this method as @link SignupDto
	 * and the User Details are Saved into the Database. A Cookie is created for the Session and returned
	 * @param signupDto Signup Details of the User
	 * @return Cookie Generated for the Session*/
	@Override
	public String signup(SignupDto signupDto) throws Exception {
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			int id = customerDao.save(connection, signupDto);
			String cookie = String.valueOf(id);
			SessionStorage.cookie = cookie;
			SessionStorage.timeout = System.nanoTime();
			return cookie;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/** Get All User Details.<br>
	 * All Users with Their Details are returned here
	 * @return List of Signed User Details
	 * */
	@Override
	public List<SignupDto> getAllUsers() throws Exception {
		List<SignupDto> users = new ArrayList<>();
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			users = customerDao.getAll(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return users;
	}

	/** Get the Users Count<br> 
	 * Number of users Signed up into the System are returned as an Integer
	 * @return Number of Signed Users
	 * */
	@Override
	public Integer getUserCount() throws Exception {
		try {
			Connection connection = DBConnection.getInstance().getConnection();
			return customerDao.getUserCount(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
