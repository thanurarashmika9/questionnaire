package com.sliitacademy.smartquiz.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.sliitacademy.smartquiz.server.service.impl.ServiceFactoryImpl;

/**
 * @author Thanura Rashmika
 * Server Application Start
*/
public class SmartServer {
	/** Main Method for Server which starts the Server Application<br>
	 * Connects Client with the Server
	 * @param args
	 * */
	public static void main(String args[]) {
		try {
			System.out.println("Service Starting...");
			Registry registry = LocateRegistry.createRegistry(8989);
			registry.rebind("SMART_QUIZ", ServiceFactoryImpl.getInstance());
			System.out.println("Service is Online");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
