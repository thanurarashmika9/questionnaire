package com.sliitacademy.smartquiz.server.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**Database Connectivity is Managed here*/
public class DBConnection {
	/** SQL Connection */
	private Connection connection;
	
	/** Instance of the DBConnection*/
    private static DBConnection dBConnection;
    
	/** Connection to the Database is Acquired in the Constructor */
    private DBConnection()throws ClassNotFoundException,SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/Smart_Quiz","root","84260899");
    }
    
	/** Static method eith Singleton Design Pattern to get a Connection to the Database 
	 * @return Existing Connection or else a New Connection to the Database
	 * @throws ClassNotFoundException
	 * @throws SQLException*/
    public static DBConnection getInstance()throws ClassNotFoundException,SQLException{
        if(dBConnection==null){
            dBConnection=new DBConnection();
        }
        return dBConnection;
    }
    
	/** Connection is Returned with this Function 
	 * @return connection to the Database*/
    public Connection getConnection(){
        return connection;
    }
}
