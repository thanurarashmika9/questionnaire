package com.sliitacademy.smartquiz.server;

import java.util.concurrent.TimeUnit;

/**Session Details are maintained here<br>
 * Session is Created for all Users and the details about the Session and Cookies are managed*/
public class SessionStorage {
	/** Cookie details*/
	public static String cookie;
	/** Timeout <b>Standby Time of the Session</b> */
	public static long timeout;
	/** Session Duration <b>Maximum duration of a Session</b>*/
	private static int sessionDuration = 2;
	
	/** function to check the Session <b>Session is checked and notifies if the Session is Expired or not
	 * by checking the Standby Time. If the Time gap is greater than the preset value, 
	 * then the Session will be identified as Expired</b>
	 * @param isAdmin Is Admin user or not
	 * @return Checks the Standby time and Returns If the Session is Expired or not
	 * */
	public static boolean checkSession(boolean isAdmin) {
		sessionDuration = isAdmin ? 5 : 2;
		long currentTime = System.nanoTime();
		long elapsedTime = currentTime - timeout;
		double timeInMinutes = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS);
		if (timeInMinutes >= sessionDuration) {
			return false;
		} else {
			timeout = System.nanoTime();
			return true;
		}
	}
}
