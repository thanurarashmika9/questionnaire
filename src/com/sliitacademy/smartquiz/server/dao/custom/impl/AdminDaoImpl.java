package com.sliitacademy.smartquiz.server.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sliitacademy.smartquiz.client.dto.AdminLoginDto;
import com.sliitacademy.smartquiz.server.dao.custom.AdminDao;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of the AdminDao which manages actions related to Database on Admin functions*/
public class AdminDaoImpl implements AdminDao {

	/** Get the Login Credentials of Admin User 
	 * @param connection  {@link DBConnection}
	 * @param userName User name of the Admin
	 * @return Login Credentials of the Admin User
	 * */
	@Override
	public AdminLoginDto getLoginCredentials(Connection connection, String userName) throws Exception {
		ResultSet rs;
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM login_detail WHERE V_USERNAME = ?");
			ps.setString(1, userName);
			rs = ps.executeQuery();
			while (rs.next()) {
				AdminLoginDto loginDto = new AdminLoginDto();
				loginDto.setId(rs.getInt("N_ID"));
				loginDto.setUserName(rs.getString("V_USERNAME"));
				loginDto.setPassword(rs.getString("V_PASSWORD"));
				return loginDto;
			}
		} catch (Exception e) {
			throw e;
		}
		return null;
	}

}
