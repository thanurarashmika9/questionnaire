package com.sliitacademy.smartquiz.server.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.sliitacademy.smartquiz.client.dto.SignupDto;
import com.sliitacademy.smartquiz.server.dao.custom.CustomerDao;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of CustomerDao which manages actions related to Database on User/ Customer functions*/
public class CustomerDaoImpl implements CustomerDao {

	/** Get the Count of Signed Users of the System
	 * @param connection  {@link DBConnection}
	 * @return Count of Signed users */
	@Override
	public Integer getUserCount(Connection connection) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		Integer count = 0;
		try {
			ps = connection.prepareStatement("SELECT COUNT(N_ID) AS cnt FROM customer_detail");
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt("cnt");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return count;
	}

	/** Get User Id by User Name 
	 * @param connection  {@link DBConnection}
	 * @param userName Username Of the User
	 * @return User Id of the User
	 * */
	@Override
	public Integer getUserId(Connection connection, String userName) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		Integer userId = 0;
		try {
			ps = connection.prepareStatement("SELECT N_ID FROM customer_detail WHERE V_NAME = ?");
			ps.setString(1, userName);
			rs = ps.executeQuery();
			while (rs.next()) {
				userId = rs.getInt("N_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userId;
	}

	/** Save Customer Details<br>
	 * @param connection  {@link DBConnection}
	 * @param signupDto Details of the Signed User
	 * @return Generated Id for the User
	 * */
	@Override
	public int save(Connection connection, SignupDto signupDto) throws Exception {
		PreparedStatement ps;
		int genId = 0;
		try {
			ps = connection.prepareStatement("INSERT INTO customer_detail VALUES(0,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, signupDto.getName());
			ps.setInt(2, signupDto.getAge());  
			ps.setInt(3, signupDto.getContact());
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
			LocalDateTime now = LocalDateTime.now();  
			ps.setString(4, String.valueOf(dtf.format(now)));
			if (ps.executeUpdate() > 0) {
				ResultSet keys = ps.getGeneratedKeys();
				if (keys.next()) {
					genId = keys.getInt(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return genId;
	}

	/** Get All User Details 
	 * @param connection  {@link DBConnection}
	 * @return List of User Details
	 * */
	@Override
	public List<SignupDto> getAll(Connection connection) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		List<SignupDto> userList = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT * FROM customer_detail");
			rs = ps.executeQuery();
			while (rs.next()) {
				SignupDto dto = new SignupDto();
				dto.setId(rs.getInt("N_ID"));
				dto.setName(rs.getString("V_NAME"));
				dto.setAge(rs.getInt("N_AGE"));
				dto.setContact(rs.getInt("N_CONTACT_NO"));
				dto.setDate(rs.getString("D_DATE_TIME"));
				userList.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return userList;
	}

}
