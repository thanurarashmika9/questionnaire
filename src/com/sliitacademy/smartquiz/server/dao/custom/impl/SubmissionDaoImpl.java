package com.sliitacademy.smartquiz.server.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.sliitacademy.smartquiz.client.dto.JsonData;
import com.sliitacademy.smartquiz.client.dto.SummaryDto;
import com.sliitacademy.smartquiz.server.dao.custom.SubmissionDao;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of SubmissionDao which manages actions related to Database on Answer Submission Functions*/
public class SubmissionDaoImpl implements SubmissionDao {

	/** Submit Answers for the Question<br> 
	 * User and Answer for Each and Every Question is Saved
	 * @param connection {@link DBConnection}
	 * @param userId Id Of the User
	 * @param questionId Id of the Question
	 * @param answerId Id of the Answer
	 * @return If Answers Saved or Not
	 * */
	@Override
	public boolean submitAnswers(Connection connection, Integer userId, Integer questionId, Integer answerId) throws Exception {
		PreparedStatement ps;
		boolean isSaved = false;
		try {
			ps = connection.prepareStatement("INSERT INTO submissions VALUES(0,?,?,?)");
			ps.setInt(1, userId);
			ps.setInt(2, questionId);
			ps.setInt(3, answerId);
			isSaved = ps.executeUpdate() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isSaved;
	}

	/** Generate the Summary for the Questionnaire 
	 * @param connection {@link DBConnection}
	 * @param questionId Id of the Question
	 * @return Summarized data for the Question
	 * */
	@Override
	public JsonData generateReport(Connection connection, int questionId) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		JsonData jsonData;
		int questionCount;
		try {
			ps = connection.prepareStatement("SELECT * FROM questions WHERE N_ID = ?");
			ps.setInt(1, questionId);
			jsonData = new JsonData();
			rs = ps.executeQuery();
			while (rs.next()) {
				jsonData.setQuestion(rs.getString("V_QUESTION_DESC"));
				jsonData.setAnswer1(rs.getString("V_ANSWER1"));
				jsonData.setAnswer2(rs.getString("V_ANSWER2"));
				jsonData.setAnswer3(rs.getString("V_ANSWER3"));
				jsonData.setAnswer4(rs.getString("V_ANSWER4"));
				jsonData.setAnswer5(rs.getString("V_ANSWER5"));
				
			}
			rs.close();
			ps.close();
			ps = connection.prepareStatement("SELECT N_QUESTION_ID, N_ANSWER_ID, COUNT(DISTINCT N_USER_ID) AS CNT FROM submissions WHERE N_QUESTION_ID = ? GROUP BY N_ANSWER_ID");
			ps.setInt(1, questionId);
			rs = ps.executeQuery();
			List<SummaryDto> users = new ArrayList<>();
			while (rs.next()) {
				SummaryDto summaryDto = new SummaryDto();
				summaryDto.setQid(rs.getInt("N_QUESTION_ID"));
				summaryDto.setAnsId(rs.getInt("N_ANSWER_ID"));
				summaryDto.setUsers(rs.getInt("CNT"));
				users.add(summaryDto);
			}
			jsonData.setUsers(users);
			return jsonData;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
