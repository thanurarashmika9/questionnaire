package com.sliitacademy.smartquiz.server.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.protocol.Resultset;
import com.sliitacademy.smartquiz.client.dto.JsonData;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.server.dao.custom.QuestionDao;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**Implementation of QuestionDao which manages actions related to Database on Question functions*/
public class QuestionDaoImpl implements QuestionDao {

	/** Update Question Function
	 * @param connection {@link DBConnection}
	 *  */
	@Override
	public boolean updateQuestion(Connection connection, QuestionDto questionDto) throws Exception {
		PreparedStatement ps;
		int index = 0;
		try {
			ps = connection.prepareStatement("UPDATE questions SET V_QUESTION_DESC = ?, \n"
					+ "V_ANSWER1 = ?, V_ANSWER2 = ?, V_ANSWER3 = ?, V_ANSWER4 = ?, V_ANSWER5 = ? \n"
					+ "WHERE N_ID = ?");
			ps.setString(++index, questionDto.getQuestion());
			ps.setString(++index, questionDto.getAnswer1());
			ps.setString(++index, questionDto.getAnswer2());
			ps.setString(++index, questionDto.getAnswer3());
			ps.setString(++index, questionDto.getAnswer4());
			ps.setString(++index, questionDto.getAnswer5());
			ps.setInt(++index, questionDto.getId());
			return ps.executeUpdate() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/** Delete Question Function 
	 * @param connection {@link DBConnection}
	 * @param questionId Id Of the Question
	 * @return If Question Deleted or not
	 * */
	@Override
	public boolean deleteQuestion(Connection connection, int questionId) throws Exception {
		PreparedStatement ps;
		try {
			ps = connection.prepareStatement("DELETE FROM questions WHERE N_ID = ?");
			ps.setInt(1, questionId);
			return ps.executeUpdate() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/** Get Number of Questions in the System 
	 * @param connection {@link DBConnection}
	 * @return Number of Questions in the System
	 * */
	@Override
	public Integer getQuestionCount(Connection connection) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		Integer count = 0;
		try {
			ps = connection.prepareStatement("SELECT COUNT(N_ID) AS cnt FROM questions");
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt("cnt");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return count;
	}

	/** Get Question Details 
	 * @param connection {@link DBConnection}
	 * @param questionId Id of the Question
	 * @return Question Details
	 * */
	@Override
	public QuestionDto getQuestion(Connection connection, int questionId) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		QuestionDto questionDto = null;
		try {
			ps = connection.prepareStatement("SELECT * FROM questions WHERE N_ID = ?");
			ps.setInt(1, questionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				questionDto = new QuestionDto();
				questionDto.setId(questionId);
				questionDto.setQuestion(rs.getString("V_QUESTION_DESC"));
				questionDto.setAnswer1(rs.getString("V_ANSWER1"));
				questionDto.setAnswer2(rs.getString("V_ANSWER2"));
				questionDto.setAnswer3(rs.getString("V_ANSWER3"));
				questionDto.setAnswer4(rs.getString("V_ANSWER4"));
				questionDto.setAnswer5(rs.getString("V_ANSWER5"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return questionDto;
	}

	/** Save new Question
	 * @param connection {@link DBConnection}
	 * @param questionDto Question Details
	 * @return Generated Id for the Question
	 *  */
	@Override
	public int save(Connection connection, QuestionDto questionDto) throws Exception {
		PreparedStatement ps;
		try {
			ps = connection.prepareStatement("INSERT INTO questions VALUES (0,?,?,?,?,?,?)");
			ps.setString(1, questionDto.getQuestion());
			ps.setString(2, questionDto.getAnswer1());
			ps.setString(3, questionDto.getAnswer2());
			ps.setString(4, questionDto.getAnswer3());
			ps.setString(5, questionDto.getAnswer4());
			ps.setString(6, questionDto.getAnswer5());
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/** Get all Questions 
	 * @param connection {@link DBConnection}
	 * @return List of Questions with Details
	 * */
	@Override
	public List<QuestionDto> getAll(Connection connection) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		List<QuestionDto> allQuestions = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT * FROM questions");
			rs = ps.executeQuery();
			while (rs.next()) {
				QuestionDto dto = new QuestionDto();
				dto.setId(rs.getInt("N_ID"));
				dto.setQuestion(rs.getString("V_QUESTION_DESC"));
				dto.setAnswer1(null == rs.getString("V_ANSWER1") ? "" : rs.getString("V_ANSWER1"));
				dto.setAnswer2(null == rs.getString("V_ANSWER2") ? "" : rs.getString("V_ANSWER2"));
				dto.setAnswer3(null == rs.getString("V_ANSWER3") ? "" : rs.getString("V_ANSWER3"));
				dto.setAnswer4(null == rs.getString("V_ANSWER4") ? "" : rs.getString("V_ANSWER4"));
				dto.setAnswer5(null == rs.getString("V_ANSWER5") ? "" : rs.getString("V_ANSWER5"));
				allQuestions.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return allQuestions;
	}
}
