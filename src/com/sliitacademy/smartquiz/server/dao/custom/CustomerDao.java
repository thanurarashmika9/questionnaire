package com.sliitacademy.smartquiz.server.dao.custom;

import java.sql.Connection;
import java.util.List;

import com.sliitacademy.smartquiz.client.dto.SignupDto;
import com.sliitacademy.smartquiz.server.dao.SuperDao;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**CustomerDao is the Class which manages actions related to Database on User/Customer related functions*/
public interface CustomerDao extends SuperDao<SignupDto, String>{

	/** Get Users Count 
	 * @param connection  {@link DBConnection}
	 * @return Number of Signed users of the System
	 * @throws Exception
	 * */
	Integer getUserCount(Connection connection) throws Exception;
	
	/** Function to get User Id by Passing User name 
	 * @param connection  {@link DBConnection}
	 * @param userName User Name of the User
	 * @return Id of the User
	 * @throws Exception
	 * */
	Integer getUserId(Connection connection, String userName) throws Exception;
}
