package com.sliitacademy.smartquiz.server.dao.custom;

import java.sql.Connection;
import java.util.List;

import com.sliitacademy.smartquiz.client.dto.JsonData;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**SubmissionDao is the Class which manages actions related to Database on Question Submission functions*/
public interface SubmissionDao {

	/** Function for AnswerSubmit 
	 * @param connection  {@link DBConnection}
	 * @param userId Id of the User
	 * @param questionId Id of the Question
	 * @param answerId Id of the Answer for the specific Question
	 * @return If the Answer is saved or not
	 * @throws Exception
	 * */
	boolean submitAnswers(Connection connection, Integer userId, Integer questionId, Integer answerId) throws Exception;

	/** Function to Generate the Report 
	 * @param connection  {@link DBConnection}
	 * @param questionId Id of the Question
	 * @return Question and Answer Data required for the Charts drawing API
	 * @throws Exception
	 * */
	JsonData generateReport(Connection connection, int questionId) throws Exception;

}