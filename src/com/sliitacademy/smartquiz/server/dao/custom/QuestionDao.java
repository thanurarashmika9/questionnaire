package com.sliitacademy.smartquiz.server.dao.custom;

import java.sql.Connection;
import java.util.List;

import com.sliitacademy.smartquiz.client.dto.JsonData;
import com.sliitacademy.smartquiz.client.dto.QuestionDto;
import com.sliitacademy.smartquiz.server.dao.SuperDao;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**QuestionDao is the Class which manages actions related to Database on Questions related functions*/
public interface QuestionDao extends SuperDao<QuestionDto, Integer> {

	/** Update Question Function
	 * @param connection  {@link DBConnection}
	 * @param questionDto Question Details
	 * @return If the Question Updated or not
	 * @throws Exception
	 * */
	public boolean updateQuestion(Connection connection, QuestionDto questionDto) throws Exception;

	/** Function to Delete Question 
	 * @param connection  {@link DBConnection}
	 * @param questionId Id Of the Question
	 * @return If the Question is Deleted or not
	 * @throws Exception
	 * */
	public boolean deleteQuestion(Connection connection, int questionId) throws Exception;

	/** Get Questions count 
	 * @param connection  {@link DBConnection}
	 * @return Number of Questions available in the System
	 * @throws Exception
	 * */
	public Integer getQuestionCount(Connection connection) throws Exception;

	/** Get a Specific Question 
	 * @param connection  {@link DBConnection}
	 * @param questionId Id of a Question
	 * @return The QUestion related to the Id
	 * @throws Exception
	 * */
	public QuestionDto getQuestion(Connection connection, int questionId) throws Exception;
}
