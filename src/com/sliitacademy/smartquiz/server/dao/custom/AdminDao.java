package com.sliitacademy.smartquiz.server.dao.custom;

import java.sql.Connection;

import com.sliitacademy.smartquiz.client.dto.AdminLoginDto;
import com.sliitacademy.smartquiz.server.db.DBConnection;

/**AdminDao is the Class which manages actions related to Database on Admin functions*/ 
public interface AdminDao {
	/** Function to get Login Credentials of the Administrator 
	 * @param connection  {@link DBConnection}
	 * @param userName userName of the Admin User
	 * @return Admin Details
	 * @throws Exception
	 * */
	public AdminLoginDto getLoginCredentials(Connection connection, String userName) throws Exception;
}
