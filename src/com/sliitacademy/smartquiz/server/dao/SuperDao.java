package com.sliitacademy.smartquiz.server.dao;

import java.sql.Connection;
import java.util.List;

/**SuperDao is having the common Methods Related to Database among the Child Classes<br>
 * Designed to minimize the Broilerplate by defining Common Methods and implementing thwm in their child classes
 * */
public interface SuperDao<T, ID> {
	
	/** Save Function<br>
	 * Common Save Function for all Saving processes
	 * @param connection DB Connection
	 * @param dto Specific Object related to the Action
	 * @return Generated Id
	 * @throws Exception*/
	public int save(Connection connection, T dto) throws Exception;
	
	/** Function to Gell all Data 
	 * @param connection DB Connection
	 * @return List contains all Objects
	 * @throws Exception
	 * */
	public List<T> getAll(Connection connection) throws Exception;
}
